import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import sectiondata from '../../data/sections.json';
import parse from 'html-react-parser';

class BannerV2 extends Component {

    componentDidMount() {

    const $ = window.$;
    
     if ($('.single-select').length){
            $('.single-select').niceSelect();
        }
  }

    render() {

        let publicUrl = process.env.PUBLIC_URL+'/'
        let imagealt = 'image'
        let data = sectiondata.banner

        const inlineStyle = {
            backgroundImage: 'url('+publicUrl+'/assets/img/recherche-formation.png)'
        }

    return <div className="banner-area jarallax" style={inlineStyle}>
            <div className="banner-inner-wrap">
            <div className="rechercher-formation-bar-top">
            <Link to="/"> <a className="annuaire">ANNUAIRE DES FORMATIONS</a></Link>
            </div>
           <Link to="/"> <div className="recherche-mot-cle">
           <img src={publicUrl+"/assets/img/mot.png"}  alt="logo" />
               <a className="mot">Recherche par mots-clés</a>
               
             </div></Link>
             <Link to="/">  <div className="recherche-par-domaine">
             <img src={publicUrl+"/assets/img/domaine.png"}  alt="logo" />
             <a className="domaine">Recherche par domaine</a>
             </div></Link>
             <Link to="/">  <div className="recherche-par-region">
             <img src={publicUrl+"/assets/img/region.png"}  alt="logo" />
             <a className="region">Recherche par region</a>
             </div></Link>


             <Link to="/recherche-communaute">  <div className="aller-communaute">
            
             <a className="com">Annuaire communauté</a>
             </div></Link>

             <Link to="/">  <div className="aller-bourse">
            
             <a className="bourse">Annuaire bourse</a>
             </div></Link>

             <Link to="/recherche-logement">  <div className="aller-logement">
            
             <a className="logement">Annuaire logement</a>
             </div></Link>
     
      
    </div>
  </div>

        }
}

export default BannerV2