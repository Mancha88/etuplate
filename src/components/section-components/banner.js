import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import sectiondata from '../../data/sections.json';
import parse from 'html-react-parser';

class Banner extends Component {

    componentDidMount() {

    const $ = window.$;
    
     if ($('.single-select').length){
            $('.single-select').niceSelect();
        }
  }

    render() {

        let publicUrl = process.env.PUBLIC_URL+'/'
        let imagealt = 'image'
        let data = sectiondata.banner

        const inlineStyle = {
          backgroundImage: 'url('+publicUrl+'/assets/img/banner/monument.png)'
        }

    return <div className="banner-area jarallax" style={inlineStyle}>
          <div className="container">
            <div className="banner-inner-wrap">
               <h5 className="sub-title">{ data.subtitle }</h5>
               <h1 className="title">{ data.title1 } <br /> { data.title2 }</h1>

               <div className="banner-inner-wrap container-search-bar-titre" >
                  <h2 className="Que-recherchez-vous">Que recherchez-vous ?</h2>
               </div>
             
               <Link to="/recherche-formation"><div className="banner-inner-wrap container-search-bar-etude" >
            <a className="bar-etable">Je recherche une école ou une université</a>
            <img className="recherche-img" src={publicUrl+"/assets/img/etude.png"} alt="logo" />
            </div></Link>
           

            <Link to="/recherche-logement"><div className="banner-inner-wrap container-search-bar-logement" >
            <a className="bar-log">Je recherche un logement</a> 
            <img className="recherche-img" src={publicUrl+"/assets/img/logement.png"} alt="logo" />
            </div></Link>

            <Link to="/recherche-communaute"><div className="banner-inner-wrap container-search-bar-consul" >
            <a className="bar-com">Ma communauté</a>  
            <img className="recherche-img" src={publicUrl+"/assets/img/consul.png"} alt="logo" />
            </div></Link>

            <Link><div className="banner-inner-wrap container-search-bar-oriente" >
            <a className="bar-par">Nos partenaires</a>
            <img className="recherche-img" src={publicUrl+"/assets/img/partenaire.png"} alt="logo" />
            </div></Link>

            </div>
          </div>
        </div>

        }
}

export default Banner