import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import sectiondata from '../../data/sections.json';
import parse from 'html-react-parser';

class BannerV3 extends Component {

    componentDidMount() {

    const $ = window.$;
    
     if ($('.single-select').length){
            $('.single-select').niceSelect();
        }
  }

    render() {

        let publicUrl = process.env.PUBLIC_URL+'/'
        let imagealt = 'image'
        let data = sectiondata.banner

        const inlineStyle = {
            backgroundImage: 'url('+publicUrl+'/assets/img/recherche-formation.png)'
        }

    return <div className="banner-area jarallax" style={inlineStyle}>
            <div className="banner-inner-wrap">
            <div className="rechercher-logement-bar-top">
            <Link to="/"> <a className="annuaire">ANNUAIRE DES LOGEMENTS</a></Link>
      </div>
      <Link to="/"> <div className="recherche-mot-cle-log">
           <img src={publicUrl+"/assets/img/mot.png"}  alt="logo" />
               <a className="mot-log">Recherche par mots-clés</a>
               
             </div></Link>
             <Link to="/">  <div className="recherche-par-type-log">
             <img src={publicUrl+"/assets/img/domaine.png"}  alt="logo" />
             <a className="type-log">Recherche par type</a>
             </div></Link>
             <Link to="/">  <div className="recherche-par-region-log">
             <img src={publicUrl+"/assets/img/region.png"}  alt="logo" />
             <a className="region-log">Recherche par region</a>
             </div></Link>

             <Link to="/recherche-communaute">  <div className="aller-communaute">
            
            <a className="com">Annuaire communauté</a>
            </div></Link>

            <Link to="/">  <div className="aller-bourse">
           
            <a className="bourse">Annuaire bourse</a>
            </div></Link>

            <Link to="/recherche-formation">  <div className="aller-formation">
           
            <a className="formation">Annuaire formation</a>
            </div></Link>
     
    </div>
  </div>

        }
}

export default BannerV3