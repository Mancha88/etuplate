import React, { Component } from 'react';
import sectiondata from '../../data/sections.json';
import parse from 'html-react-parser';
import { Link } from 'react-router-dom';

class Registration extends Component {


    render() {

        let publicUrl = process.env.PUBLIC_URL+'/'
        let imagealt = 'image'
        let data = sectiondata.whychooseus


    return <div className="page-inscription">
              <div className="image">
                  <Link to="/connexion"><div className="etude-au-senegal"> Etudes au Sénégal</div></Link>
              </div>
              <div className="je-crais-mon-compte">
              <a className="consignes">Les champs marqués d'un astérisque (*) doivent être renseignés</a>
               <a className="titre-addresse">Adresse électronique</a>

              </div>
               
              <div className="adresse-electronique">
              <a className="info-addresse-electronique">Cette adresse me servira d'identifiant pour accéder à mon compte, et, si je l'accepte, pour recevoir des messages d'information de la part des etablissement scolaire auprès desquels je souhaite candidater, des gerants de biens immobilier et de mon consulat.</a>
          
              <form className="adresse-form">
                <label>
                Adresse électronique * :
               <input type="text" name="name" />
                </label>
                </form>
                <form className="adresse-form-conf">
                <label>
                Confirmation de l'adresse électronique * :
               <input type="text" name="name" />
                </label>
                </form>

              </div>
              <div className="identite">
              <a className="titre-identite">Identité</a>
              <a className="format-date">format:jj/mm/aaaa</a>
              <form className="nom">
                <label>
                Nom de famille * :
               <input type="text" name="name" />
                </label>
                </form>

                <form className="autre-nom">
                <label>
                Autres noms (patronyme) :
               <input type="text" name="name" />
                </label>
                </form>

                <form className="prenom">
                <label>
                Prenom * :
               <input type="text" name="prenom" />
                </label>
                </form>

                <form className="sexe">
                <label>
                Sexe * :
                   <select>
                        <option ></option>
                        <option >Masculin</option>
                        <option >Féminin</option>
                        <option >Non précisé</option>
                        
                    </select>
                </label>
                </form>

                <form className="date-naissance">
                <label>
                Date de naissance * :
               <input type="text" name="Date de naissance" />
                </label>
                </form>

                <form className="lieu-naissance">
                <label>
                Lieu de naissance * :
               <input type="text" name="Lieu de naissance" />
                </label>
                </form>

                <form className="pays-naissance">
                <label>
                Pays de naissance * :
                    <select>
                        <option ></option>
                        <option >Sénégal</option>
                        <option >Cote d'ivoire</option>
                        <option >Mali</option>
                        <option >Burkina Faso</option>
                    </select>
                </label>
                </form>

                <form className="pays-nationalite">
                <label>
                Pays de nationalité * :
                     <select>
                        <option ></option>
                        <option >Sénégal</option>
                        <option >Cote d'ivoire</option>
                        <option >Mali</option>
                        <option >Burkina Faso</option>
                    </select>
                </label>
                </form>

              </div>

              <div className="divers">
                <a className="titre-divers">Divers</a>
                 
                

              </div>
              <div className="validation">
                 <button class="btn btn-primary" type="submit">Créer mon compte</button>
              </div>
             
          </div>
          
        }
}

export default Registration